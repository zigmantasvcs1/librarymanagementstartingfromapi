﻿using LibraryApi.Models;
using Microsoft.AspNetCore.Mvc;

namespace LibraryApi.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class ReaderController : ControllerBase
	{
		private static readonly List<ReaderDto> readers = new List<ReaderDto>
		{
			new ReaderDto
			{
				Id = 1,
				Name = "Jonas Jonaitis",
				Email = "jonaitis@example.com",
				RegistrationDate = DateTime.Now.AddYears(-1)
			},
			new ReaderDto
			{
				Id = 2,
				Name = "Jonas Jonaitis",
				Email = "jonaitis@example.com",
				RegistrationDate = DateTime.Now.AddMonths(-6)
			}
		};

		[HttpGet("GetAllReaders")]
		public async Task<IActionResult> GetAllReaders()
		{
			await Task.Delay(100); // temporary stuff to keep it async until we connect other parts
			return Ok(readers);
		}

		[HttpPost("RegisterNewReader")]
		public async Task<IActionResult> RegisterNewReader([FromBody] ReaderDto newReader)
		{
			await Task.Delay(100); // temporary stuff to keep it async until we connect other parts

			// Simulate adding a new reader with a new ID
			newReader.Id = readers.Count + 1;
			readers.Add(newReader);

			return CreatedAtAction(
				nameof(GetAllReaders), 
				new { id = newReader.Id }, 
				newReader
			);
		}
	}
}
