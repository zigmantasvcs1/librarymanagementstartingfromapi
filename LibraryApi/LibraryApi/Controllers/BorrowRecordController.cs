﻿using LibraryApi.Models;
using Microsoft.AspNetCore.Mvc;

namespace LibraryApi.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class BorrowRecordController : ControllerBase
	{
		private static readonly List<BorrowRecordDto> borrowRecords = new List<BorrowRecordDto>
		{
			new BorrowRecordDto
			{
				Id = 1,
				BookId = 1,
				ReaderId = 1,
				BorrowDate = DateTime.Now.AddDays(-10),
				ReturnDate = null
			},
			new BorrowRecordDto
			{
				Id = 2,
				BookId = 2,
				ReaderId = 1,
				BorrowDate = DateTime.Now.AddDays(-5),
				ReturnDate = null
			},
		};

		[HttpGet("GetBorrowRecordsByReader/{readerId}")]
		public async Task<IActionResult> GetBorrowRecordsByReader(int readerId)
		{
			await Task.Delay(100); // temporary stuff to keep it async until we connect other parts

			var records = borrowRecords
				.Where(br => br.ReaderId == readerId)
				.ToList();

			if (records == null || records.Count == 0)
			{
				return NotFound($"No borrow records found for reader with ID {readerId}.");
			}
			return Ok(records);
		}

		[HttpPost("BorrowBook")]
		public async Task<IActionResult> BorrowBook([FromBody] BorrowRecordDto borrowBook)
		{
			await Task.Delay(100); // temporary stuff to keep it async until we connect other parts

			// Simulate creating a new borrow record
			var newBorrowRecord = new BorrowRecordDto
			{
				Id = borrowRecords.Max(br => br.Id) + 1,
				BookId = borrowBook.BookId,
				ReaderId = borrowBook.ReaderId,
				BorrowDate = DateTime.Now,
				ReturnDate = null
			};

			borrowRecords.Add(newBorrowRecord);

			return CreatedAtAction(
				nameof(GetBorrowRecordsByReader),
				new { readerId = borrowBook.ReaderId },
				newBorrowRecord
			);
		}
	}
}
