﻿using LibraryApi.Models;
using Microsoft.AspNetCore.Mvc;

namespace LibraryApi.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class BookController : ControllerBase
	{
		private static readonly List<BookDto> Books = new List<BookDto>
		{
			new BookDto
			{
				Id = 1,
				Title = "Book One",
				Author = "Author A",
				IsAvailable = true
			},
			new BookDto
			{
				Id = 2,
				Title = "Book Two",
				Author = "Author B",
				IsAvailable = true
			},
			new BookDto
			{
				Id = 3,
				Title = "Book Three",
				Author = "Author C",
				IsAvailable = false
			}
		};

		[HttpGet("GetAvailableBooks")]
		public async Task<IActionResult> GetAvailableBooks()
		{
			await Task.Delay(100); // temporary stuff to keep it async until we connect other parts

			var availableBooks = Books.FindAll(book => book.IsAvailable);
			return Ok(availableBooks);
		}

		[HttpPost("ReserveBook")]
		public async Task<IActionResult> ReserveBook(int bookId, int readerId)
		{
			await Task.Delay(100); // temporary stuff to keep it async until we connect other parts

			// fake implementation
			var book = Books.Find(b => b.Id == bookId);

			if (book != null && book.IsAvailable)
			{
				book.IsAvailable = false; // Simulate reserving the book
				return StatusCode(204); // No Content to indicate success without returning data
			}

			return NotFound("Book not found or is not available");
		}

		[HttpPost("ReturnBook")]
		public async Task<IActionResult> ReturnBook(int bookId)
		{
			await Task.Delay(100); // temporary stuff to keep it async until we connect other parts

			// fake implementation
			var book = Books.Find(b => b.Id == bookId);

			if (book != null)
			{
				book.IsAvailable = true; // Simulate returning the book
				return StatusCode(204); // No Content to indicate success without returning data
			}

			return NotFound("Book not found");
		}
	}
}
