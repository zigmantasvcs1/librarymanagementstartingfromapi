﻿namespace LibraryApi.Models
{
	public class ReaderDto
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string Email { get; set; }
		public DateTime RegistrationDate { get; set; }
	}
}
